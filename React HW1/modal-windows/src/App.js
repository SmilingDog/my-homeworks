import React, { Component } from "react";
import "./App.scss";
import Button from "./components/Button/Button";
import Message from "./components/Message/Message";
import Modal from "./components/Modal/Modal";

class App extends Component {
  state = {
    showModalOne: false,
    showModalTwo: false,
    okFirstClicked: false,
    okSecondClicked: false,
  };

  toggleModalOne = () => {
    this.setState({ showModalOne: !this.state.showModalOne });
  };
  toggleModalTwo = () => {
    this.setState({ showModalTwo: !this.state.showModalTwo });
  };
  handleClickOutside = (event) => {
    const outside = document.getElementById("body");
    if (event.target === outside) {
      this.setState({ showModalOne: false });
      this.setState({ showModalTwo: false });
    }
  };
  onCancel = () => {
    const { showModalOne} = this.state;
    if (showModalOne) {
      this.setState({ showModalOne: false });
    } else {
      this.setState({ showModalTwo: false });
    }
  };
  onOk = () => {
    const { showModalOne } = this.state;
    if (showModalOne) {
      this.setState({ okFirstClicked: true });
      this.setState({ showModalOne: false });
    } else {
      this.setState({ okSecondClicked: true });
      this.setState({ showModalTwo: false });
    }
  };
  render() {
    const { showModalOne, showModalTwo, okFirstClicked, okSecondClicked } = this.state;
    const header = "Do you want to delete first file ?";
    const header2 = "Do you want to delete React ?";
    const text =
      "Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?";
    const text2 =
      "Once you stop practice React, it won't be possible to become a top IT-specialist. Are you sure you want to quit?";
    const btnOk = <button className="modal__btn" onClick={this.onOk}>OK</button>;
    const modalBtnCancel = <button className="modal__btn" onClick={this.onCancel}>Cancel</button>;
    const title1 = "The file was deleted!";
    const title2 = 'The React was deleted!';

    return (
      <div
        className={showModalOne || showModalTwo ? "App dark" : "App"}
        onClick={this.handleClickOutside}
        id="body"
      >
        <Button
          backgroundColor="yellow"
          text={showModalOne ? "Close First Modal" : "Open First Modal"}
          fontSize="32px"
          onClick={this.toggleModalOne}
        />
        <Button
          backgroundColor="skyblue"
          text={showModalTwo ? "Close Second Modal" : "Open Second Modal"}
          fontSize="32px"
          onClick={this.toggleModalTwo}
        />
        {showModalOne && (
          <Modal
            header={header}
            closeButton
            text={text}
            backgroundColor="rgb(241, 91, 71)"
            color="#fff"
            btnOk={btnOk}
            modalBtnCancel={modalBtnCancel}
          />
        )}
        {showModalTwo && (
          <Modal
            header={header2}
            closeButton
            text={text2}
            backgroundColor="green"
            color="#fff"
            btnOk={btnOk}
            modalBtnCancel={modalBtnCancel}
          />
        )}
        {okFirstClicked && <Message title={title1} />}
        {okSecondClicked && <Message title={title2} />}
      </div>
    );
  }
}

export default App;
