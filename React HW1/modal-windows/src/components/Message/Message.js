import React, { PureComponent } from "react";

class Message extends PureComponent {
  render() {
    const { title } = this.props;
    return <h2>{title}</h2>;
  }
}
export default Message;
