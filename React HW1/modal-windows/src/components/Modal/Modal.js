import React, { PureComponent } from "react";
import CrossIcon from "../Icons/Icon-CrossIcon";
import "./Modal.scss";

class Modal extends PureComponent {
  render() {
    const { header, text, backgroundColor, color, closeButton, btnOk, modalBtnCancel } = this.props;

    const styles = {
      backgroundColor,
      color,
    };

    return (

      <div style={styles} className="modal">
        <div>
          <div className="modal__header">
            <h2 className="modal__header__text">{header}</h2>
            {closeButton && <CrossIcon />}
          </div>
          <p className="modal__text">{text}</p>
        </div>
        {btnOk}
        {modalBtnCancel}
      </div>
    );
  }
}

export default Modal;
