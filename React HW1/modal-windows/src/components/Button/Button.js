import React, { Component } from "react";
import './Button.scss'

class Button extends Component {
  render() {
    const { backgroundColor, text, fontSize, onClick } = this.props;

    const styles = {
      backgroundColor,
      fontSize,
    };

    return (
      <button style={styles} onClick={onClick} className="btn">
        {text}
      </button>
    );
  }
}

export default Button;
