import React, { PureComponent } from "react";
import "./Cross.scss";

class CrossIcon extends PureComponent {
  render() {
    return (
      <div className="cross">
        <span>&times;</span>
      </div>
    );
  }
}

export default CrossIcon;
