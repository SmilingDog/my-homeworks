const btn = document.getElementById("paint");

btn.addEventListener("click", paintCircles);

function paintCircles() {
  const form = document.createElement("form");
  form.innerHTML = `
        <label for="diameter">Диаметр круга:</label>
        <input type="text" id="diameter" name="diameter" />
        <button type="submit">Нарисовать</button>`;

  btn.after(form);

  // const form = document.getElementById("form");

  form.addEventListener("submit", (e) => {
    e.preventDefault();
    const parent = document.createElement("div");
    parent.style.display = "flex";
    parent.style.flexFlow = "row wrap";

    const colors = ["#f00", "#0f0", "#00f", "#ff0", "#0ff", "#f0f"];

    for (let i = 0; i < 50; i++) {
      const diameter = document.getElementById("diameter");
      const circle = document.createElement("div");
      circle.style.width = `${diameter.value}px`;
      circle.innerText = i + 1;
      circle.style.height = `${diameter.value}px`;
      circle.style.borderRadius = "50%";
      let index = Math.floor(Math.random() * colors.length);
      let randomColor = colors[index];
      circle.style.backgroundColor = `${randomColor}`;
      circle.style.display = "flex";
      circle.style.justifyContent = "center";
      circle.style.alignItems = "center";
      parent.append(circle);
    }
    form.after(parent);
    document.body.addEventListener("click", (e) => {
      if (e.target.childNodes.length > 1) return;
      e.target.closest("div").remove();
    });
  });
}
