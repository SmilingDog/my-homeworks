
const workSpeed = [ 2, 5, 4, 2, 3] //*в команде 5 девов. Числа - это стори поинтов/в день каждого
const tasks = [8, 12, 16, 23, 9, 10, 40, 100 ] //*кол-во задача в беклоге  - 7. Числа - это стори пооинт длязавершения
const deadline = "16 Sep 2021"


function sprint(speed, backLog, date) {
    //todo 1) Посчитать общую производительность команды в день
    let dailyTeamProductivity = speed.reduce((acc, item) => acc + item)
    let totalTaskPoints = backLog.reduce((acc, item) => acc + item)

    //todo 2) Посчитать количество дней на выолнение всех заданий
    let daysToCompleteTasks = Math.floor(totalTaskPoints / dailyTeamProductivity)
    console.log(daysToCompleteTasks);

    //todo 3) Посчитать количество рабочих дней от сегодня до дедлайна
    const today = new Date()
    let deadline = new Date(date)

    let daysToDeadline = Math.ceil((deadline - today) / (24*60*60*1000)) //*считаем сколько всего дней до дедлайна
    let weekEnds = daysToDeadline / 7 * 2 //* подсчитываем количество выходных на все дни
    let realDaysToDeadline = Math.ceil(daysToDeadline - weekEnds) //*учитываем выходные в графике(отнимаем)
    console.log(realDaysToDeadline);

    daysToCompleteTasks < realDaysToDeadline ? console.log(`Все задачи будут успешно выполнены за ${realDaysToDeadline - daysToCompleteTasks} дней до наступления дедлайна!`) : console.log(`Команде разработчиков придется потратить дополнительно ${daysToCompleteTasks - realDaysToDeadline} дней после дедлайна, чтобы выполнить все задачи в беклоге`)

}

sprint(workSpeed, tasks, deadline)
