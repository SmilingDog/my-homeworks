"use strict"

const theme = document.querySelector('.toggle-theme');
const form = document.querySelector('.main-form');
const inputs = document.querySelectorAll('.form-input');
const or = document.querySelector('span')
const submit = document.querySelector('.form-submit');
let darkTheme = localStorage.getItem('darkTheme');

if (darkTheme=="enabled"){
    themeToggle();
}
//обязательно надо проверить память перед манипуляциями;

const changeTheme = () => {
    themeToggle();
    if (darkTheme == null || darkTheme == "null") {
        localStorage.setItem('darkTheme', 'enabled');
    } else {
        localStorage.setItem('darkTheme', "null");
    }
}
theme.addEventListener('click', changeTheme);

function themeToggle(){
    form.classList.toggle('grey-form');
    or.classList.toggle('alt-style');
    submit.classList.toggle('alt-submit');
    for(let input of inputs) {
        input.classList.toggle('green-input');
    }
}
