"use strict"

const   input = document.querySelector('input'),
        span = document.createElement('span'),
        spanX = document.createElement('span'),
        form = document.querySelector('form'),
        warning = document.createElement('p');

document.body.prepend(spanX);
document.body.prepend(span);
form.append(warning);

input.addEventListener("focus", function () {
input.style.borderColor = "rgb(48, 192, 19)";
input.style.outline = 'none';
});

input.addEventListener("blur", function () {
input.style.border = "5px solid #000";
});

input.addEventListener('input', showButtons);
input.addEventListener('input', negativeBan);

function showButtons(val) {
    input.addEventListener("blur", function () {
        span.textContent = `Текущая цена: ${val.target.value}`;
        input.style.color = "rgb(48, 192, 19)";
        span.setAttribute('class','buttons');
        spanX.textContent = "X";
        spanX.setAttribute('class','buttons');

    }
)}

function negativeBan (val) {
    if (val.target.value < 0) {
        span.style.display = "none";
        input.addEventListener("blur", function () {
            input.style.border = "5px solid red";
            input.style.color = "red";
            warning.textContent = "Please enter correct price!";
        }
    )}
}
spanX.addEventListener('click', hideButtons);

function hideButtons () {
    span.remove();
    spanX.remove();
    input.value = '';
    warning.textContent = '';
    input.style.border = "5px solid #000";
}
