"use strict"

let names = ['Оля', 'Ева', 'Максик', 'Леша', 'Женя', 'Сегодня у Макса день рождения!'];


let content = names.map(element => `<li>${element} <button>&times;</button></li>`).join("");
document.body.innerHTML = `<ul>${content}</ul>`

const buttons = document.querySelectorAll('button')
const ul = document.querySelector('ul')

buttons.forEach(btn => btn.addEventListener('click', () => {
    btn.parentNode.remove()
    if(ul.childNodes.length == 0) ul.remove()
}));
