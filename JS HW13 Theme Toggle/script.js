const change = document.getElementById('theme')
const body = document.body
const navLinks = document.querySelectorAll('.header-menu-link')
const text = document.querySelectorAll('.paragraph')
const title = document.querySelector(".heading")
const btn = document.querySelector('.button')

const toggleTheme = () => {
    let elements = [body, title, btn]
    elements.forEach(el => el.classList.toggle('variant'))
    navLinks.forEach(item => item.classList.toggle('variant'))
    text.forEach(paragraph => paragraph.classList.toggle('variant'))
    btn.innerText === 'GET STARTED' ? btn.innerText = 'WELCOME' : btn.innerText = 'GET STARTED'
    change.innerText === 'DARK THEME' ? change.innerText = 'LIGHT THEME' : change.innerText = 'DARK THEME'
}

change.addEventListener('click', toggleTheme)
