"use strict"

function filterShort(arr, dataType) {
    return arr.filter((element) => typeof element !== dataType);
}

console.log(filterShort(['hello', 'world', 23, '23', true, 1, {a:1, b:2}], 'string'));
