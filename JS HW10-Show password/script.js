"use strict"
const password = document.querySelector('input');
const confirm = document.getElementById('confirm');
const openEye = document.getElementById('eye-open');
const closedEye = document.getElementById('eye-closed');
const btn = document.querySelector('.btn');

openEye.addEventListener('click', function() {
    openEye.classList.toggle('fa-eye-slash');
    if(password.type === "password"){
        password.type = "text";
    }
    else {
        password.type = "password";
    }

});

closedEye.addEventListener('click', function() {

    if(confirm.type === "password"){
        confirm.type = "text";
        closedEye.classList.add('fa-eye');
        closedEye.classList.remove('fa-eye-slash');
    }
    else {
        confirm.type = "password";
        closedEye.classList.remove('fa-eye');
        closedEye.classList.add('fa-eye-slash');
    }

});

btn.addEventListener('click', function (event) {
    let warningBox = document.querySelector('.warning-box');
    if( password.value === confirm.value) {

        if (warningBox.firstElementChild) warningBox.firstElementChild.remove();
        setTimeout (alert, 200, 'You are welcome!');
    }
    else {
        const warning = document.createElement('p');
        warning.classList.add('warn')
        warning.textContent = 'Нужно ввести одинаковые значения';
        warning.style.color = 'red';
        let txt = document.querySelector('.warn');
        if(!txt) {
           warningBox.appendChild(warning);
        } else if(txt) {
            warning.remove();
        }
    }
})
