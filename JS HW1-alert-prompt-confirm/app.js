"use strict"

let name = prompt("What is your name?");
while (+name) {
     name = prompt("Please enter valid name");
}

let age = prompt("How old are you?");
while (isNaN(+age)) {
    age = prompt("Please enter valid age");
}

if (age > 22) {
    alert(`Welcome, ${name}!`);
}

else if (age < 18) {
    alert("You are not allowed to visit this website");
}

else if (18 <= age <= 22) {
    let mature = confirm("Are you sure you want to continue?");
    if (mature) {
        alert (`Welcome, ${name}!`);
    }
    else {
        alert("You are not allowed to visit this website");
    }
}
