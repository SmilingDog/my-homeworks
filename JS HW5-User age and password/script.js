"use strict"

function createNewUser() {

const newUser = {
    set firstName(value) {
        this._firstName = value;
    },
    get firstName() {
        return this._firstName;
    },
    set lastName(value) {
        this._lastName = value;
    },
    get lastName() {
        return this._lastName;
    },
    set birthday(value) {
        this._birthday = value;
    },
    get birthday() {
        return this._birthday;
    },
    getPassword: function () {
        return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(0, 4);
    },
    getAge: function () {
        let today = new Date();
        let birthDate = new Date (this.birthday);
        console.log(birthDate);
        let age = today.getFullYear() - birthDate.getFullYear();
        let m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }
}
newUser.firstName = prompt("Enter your Name");
newUser.lastName = prompt("Enter your Last Name");
newUser.birthday = prompt("Your birthday: in dd.mm.yyyy format").split('.').reverse().join('-');
    return newUser;

}

let user = createNewUser();

console.log(user.getPassword());
console.log(user.getAge());
