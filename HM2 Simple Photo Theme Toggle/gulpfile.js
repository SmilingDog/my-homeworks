//* Gulp - менеджер задач и сборщик. Обрабатывает html, css, js файлы, минифицирует и собирает в билд. Преобразовывает scss(препроцессор) в css, добавляет автопрефиксы?, склеивает части html страниц

const { src, dest, series, watch } = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const csso = require("gulp-csso");
const include = require("gulp-file-include");
const htmlmin = require("gulp-htmlmin");
const autoprefixer = require("gulp-autoprefixer");
const concat = require("gulp-concat");
const sync = require("browser-sync").create();
const del = require("del");
const image = require('gulp-image');


//*Прописываем Tasks-Functions: 1) склеить части html

function html() {
  return src("src/**.html")
    .pipe(include({ prefix: "@@" }))
    .pipe(htmlmin({ collapseWhitespace: true}))
    .pipe(dest("dist"));
}

//* SCSS Task: преобразовать в css

function scss() {
  return src("src/scss/**.scss")
    .pipe(sass())
    .pipe(autoprefixer({cascade: false}))
    .pipe(csso())
    .pipe(concat("styles.css"))
    .pipe(dest("dist"));
}

//* Clear dist folder Task
function clearDist() {
  return del("dist");
}
//* Task to transfer JS from src to dist
function scripts() {
	return src("./src/js/**.js")
	.pipe(concat('script.js'))
	.pipe(dest("dist"))

}
//* Task to minify all pictures
function imgSquash() {
  return src("./src/img/*.png")
  .pipe(image())
  .pipe(dest("dist/img"));
}
//* Task for Live reload of dev page after changes
const serve = () => {
  sync.init({
    server: "./dist",
  });
  watch('src/**/*.html', series(html)).on("change", sync.reload)
  watch('src/scss/*.scss', series(scss)).on("change", sync.reload)
  watch('src/js/*.js', series(scripts)).on("change", sync.reload )
};

exports.build = series(clearDist, html, scss, imgSquash, scripts);
exports.dev = series(clearDist, html, scss, imgSquash, scripts, serve)
exports.clearDist = clearDist;
