const toggle = document.getElementById('toggle-button')
const body = document.body
const theme = document.getElementById('theme')
const navLinks = document.querySelectorAll('.header-menu-link')
const text = document.querySelectorAll('.paragraph')
const title = document.querySelector(".heading")
const btn = document.querySelector('.button')

const toggleTheme = () => {
    let elements = [body, btn, theme, title]
    elements.forEach(el => el.classList.toggle('dark'))
    navLinks.forEach(item => item.classList.toggle('dark'))
    text.forEach(paragraph => paragraph.classList.toggle('dark'))
    btn.innerText === 'GET STARTED' ? btn.innerText = 'WELCOME' : btn.innerText = 'GET STARTED'
    theme.innerText === 'Dark' ? theme.innerText = 'Light' : theme.innerText = 'Dark'
}

toggle.addEventListener('click', toggleTheme)
