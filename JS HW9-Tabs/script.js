"use strict";

const tabs = document.querySelectorAll(".tabs-title");
console.log(tabs);
const tabs__text = document.querySelectorAll(".tabs-text");

tabs.forEach(tab => tab.addEventListener("click", () => {

    tabs.forEach((tab) => tab.classList.remove("active-tab"));
    tabs__text.forEach((block) => block.classList.remove("active-text"));

    tab.classList.add("active-tab");
    const hero = tab.getAttribute("data-hero");
    document.getElementById(hero).classList.add("active-text");
  })
);
