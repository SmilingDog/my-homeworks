$(document).ready(function(){
    $('.tabs-title').click(function(){
        $('.tabs-title').removeClass('active-tab');
        $(this).toggleClass('active-tab');
        $('.tabs-text').removeClass('active-text');
        $('#'+$(this).attr('data-hero')).toggleClass('active-text');
    })
})
