const reqURL = "https://swapi.dev/api/films/";
const root = document.querySelector(".root");
const filmList = document.createElement("ul");

starWarsPersonageInfo(reqURL);

function showLoader() {
  const loaderBox = document.createElement("div");
  const loader = document.createElement("span");
  loaderBox.className = "loader";
  loader.textContent = "Loading...";
  loaderBox.append(loader);
  return loaderBox;
}

async function starWarsPersonageInfo(reqURL) {
  root.append(showLoader());
  try {
    const response = await fetch(reqURL);
    const data = await response.json();
    if (data) {
      document.querySelector(".loader").remove();
    }
    data.results.forEach((el) => {
      const { title, characters, episode_id, opening_crawl } = el;
      const filmItem = document.createElement("li");
      const filmTitle = document.createElement("h2");
      filmTitle.textContent = `${episode_id} - ${title}`;
      const filmNotes = document.createElement("p");
      filmNotes.textContent = opening_crawl;
      const filmPersonages = document.createElement("div");
      filmPersonages.textContent = "Following personages play in the film:";
      filmItem.append(filmTitle, filmNotes, filmPersonages);

      characters.forEach(async (url) => {
        const reply = await fetch(url);
        const personInfo = await reply.json();
        const personages = document.createElement("span");
        personages.style.margin = "20px 0";
        personages.textContent = personInfo.name + ",  ";
        personages.className = "personages";
        filmItem.append(personages);
      });
      filmList.append(filmItem);
      root.append(filmList);
    });
  } catch (err) {
    console.error(err)
  }
}
