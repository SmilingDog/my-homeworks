"use strict"

let a = +prompt("Пож-та, введите 1-ое число");

while (a == null || a == undefined || a =="" || isNaN(+a)) {
   console.log("⛔️ Ошибка! Аргумент не является числом!");
   a = +prompt("Пож-та, введите 1-ое число");

}
let b = +prompt("Пож-та, введите 2-ое число");

while (b == null || b == undefined || b =="" || isNaN(+b)) {
   console.log("⛔️ Ошибка! Аргумент не является числом!");
   b = +prompt("Пож-та, введите 2-ое число");
}
let operation  = prompt("Пож-та, введите матем. операцию: + - * /");

function signIsValid(sign) {
   return sign == "+" || sign == "-" || sign == "*" || sign == "/"
}
while (!signIsValid(operation)) {
   operation = prompt("Пож-та, ТОЛЬКО знаки операций: + - * /");
}

function mathOperation (a, b, operation) {

   switch (operation) {
      case "+":
         return a + b;

      case "-":
         return a - b;

      case "*":
         return a * b;

      case "/":
         return a / b;

      default:
         return "Impossible to calculate";
   }
}

let result = mathOperation (a, b, operation);
console.log(`Результат Вашей операции: ${result}`);
