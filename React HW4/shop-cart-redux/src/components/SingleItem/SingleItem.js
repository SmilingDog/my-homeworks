import React, { useEffect } from "react";
import styles from "./SingleItem.module.css";
import { connect, useDispatch } from "react-redux";
import { addToCartAction, loadCurrentItemAction } from "../../redux/Shopping/shopping-actions";
import { useParams } from "react-router";

const SingleItem = ({ currentItem, addToCart, cart, products }) => {
  const currentID = useParams().id; //todo - считали id из адресной строки
  const dispatch = useDispatch();
  const product = products.find((p) => p.id === currentID); //todo - нашли продукт в Сторе с данным id (currentID)
  useEffect(() => {
    dispatch(loadCurrentItemAction(product));
  }, [dispatch, product]); //todo - загрузили этот продукт в переменную currentItem  и отрендерили SingleItem компонент

  const inCart = cart.find((item) => item.id === currentItem.id);

  return (
    <div className={styles.singleItem}>
      <img
        className={styles.singleItem__image}
        src={currentItem.image}
        alt={currentItem.title}
        width="340"
        height="340"
      />
      <div className={styles.singleItem__details}>
        <p className={styles.details__title}>{currentItem.title}</p>
        <p className={styles.details__description}>{currentItem.description}</p>
        <p className={styles.details__price}>UAH {currentItem.price}</p>
        {!inCart ? (
          <button
            onClick={() => addToCart(currentItem.id)}
            className={styles.details__addBtn}
          >
            Add To Cart
          </button>
        ) : (
          <button
            onClick={() => addToCart(currentItem.id)}
            className={`${styles.details__addBtn} ${styles.buttons__incart}`}
          >
            In Cart
          </button>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    currentItem: state.shop.currentItem,
    cart: state.shop.cart,
    products: state.shop.products,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addToCart: (id) => dispatch(addToCartAction(id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SingleItem);
