import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import styles from "./Navbar.module.css";

import { connect } from "react-redux";

const Navbar = ({ cart, favorites}) => {
  const [cartCount, setCartCount] = useState(0);
  const [favsCount, setFavsCount] = useState(0);


  useEffect(() => {
    let count1 = 0;
    let count2 = 0;
    cart.forEach((item) => {
      count1 += item.qty;
    });
    favorites.forEach((item) => {
      count2 += item.qty;
    })
    setCartCount(count1);
    setFavsCount(count2)
  }, [cart, cartCount, favsCount, favorites]);

  return (
    <header className={styles.navbar}>
      <NavLink exact to="/" className={styles.link} activeClassName={styles.link__active}>
        <h2>Sportify Shop Home</h2>
      </NavLink>
      <NavLink to="/cart" className={styles.navbar__cart} activeClassName={`${styles.navbar__cart} ${styles.active}`}>
        <div className={styles.row}>
          <h3 className={styles.cart__title}>Cart</h3>
          <img
            className={styles.cart__image}
            src="https://image.flaticon.com/icons/svg/102/102276.svg"
            alt="shopping cart"
          />
          <div className={styles.cart__counter}>{cartCount}</div>
        </div>
      </NavLink>
      <NavLink to="/favorites" className={styles.navbar__cart} activeClassName={`${styles.navbar__cart} ${styles.active}`} >
        <div className={styles.row}>
          <h3 className={styles.cart__title}>Favorites</h3>
          <img
            className={styles.cart__image}
            src="https://img.flaticon.com/icons/png/512/561/561238.png"
            alt="favorites"
          />
          <div className={styles.cart__counter}>{favsCount}</div>
        </div>
      </NavLink>
    </header>
  );
};

const mapStateToProps = (state) => {
  return {
    cart: state.shop.cart,
    favorites: state.shop.favorites
  };
};

export default connect(mapStateToProps)(Navbar);
