import React from "react";
import { Link } from "react-router-dom";
import style from "./PageNotFound.module.css";

function PageNotFound() {
  return (
    <div className={style.text_center}>
      <h1 className={style.color}>PAGE NOT FOUND</h1>
      <Link to="/">Back to Main page</Link>
    </div>
  );
}

export default PageNotFound;
