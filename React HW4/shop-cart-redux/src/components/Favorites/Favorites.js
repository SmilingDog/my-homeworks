import React from "react";
import { connect } from "react-redux";
import styles from "./Favorites.module.css";
import { openFavoritesModalAction } from "../../redux/Modals/modal-actions";
import FavoritesModal from "../Modals/FavoritesModal";
import { Link } from "react-router-dom";

function Favorites({ favorites, openFavoritesModal }) {
  return (
    <div>
      <FavoritesModal />
      {!favorites.length && (
        <h2 className={styles.center}>Favorites is empty!</h2>
      )}
      {favorites.map((item) => (
        <div className={styles.cartItem} key={item.id}>
          <img
            className={styles.cartItem__image}
            src={item.image}
            alt={item.title}
          />
          <div className={styles.cartItem__details}>
            <Link to={`/product/${item.id}`}>
              <p className={styles.details__title}>{item.title}</p>
            </Link>
            <p className={styles.details__desc}>{item.description}</p>
            <p className={styles.details__price}>UAH {item.price}</p>
          </div>
          <button
            onClick={() => openFavoritesModal(item)}
            className={styles.actions__deleteItemBtn}
          >
            <img
              src="https://image.flaticon.com/icons/svg/709/709519.svg"
              alt="delete icon"
            />
          </button>
        </div>
      ))}
    </div>
  );
}
const mapStateToProps = (state) => {
  return {
    favorites: state.shop.favorites,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    openFavoritesModal: (item) => dispatch(openFavoritesModalAction(item)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Favorites);
