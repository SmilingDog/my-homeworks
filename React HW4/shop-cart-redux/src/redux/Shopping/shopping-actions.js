import {
  SET_PRODUCTS,
  IS_LOADING,
  LOAD_CURRENT_ITEM,
  ADD_TO_CART,
  ADJUST_ITEM_QTY,
  REMOVE_FROM_CART,
  ADD_TO_FAVORITES,
  REMOVE_FROM_FAVORITES,
} from "./shopping-types";
import axios from "axios";

export const loadData = () => (dispatch) => {
  dispatch({ type: IS_LOADING, payload: true });
  setTimeout(() => {
    axios("/products.json").then((res) => {
      console.log(res.data);
      dispatch({ type: SET_PRODUCTS, payload: res.data });
      dispatch({ type: IS_LOADING, payload: false });
    });
  }, 3000);
};
export const loadCurrentItemAction = (product) => {
  return {
    type: LOAD_CURRENT_ITEM,
    payload: product,
  };
};
export const addToCartAction = (itemID) => {
  return {
    type: ADD_TO_CART,
    payload: itemID,
  };
};

export const removeFromCartAction = (itemID) => {
  return {
    type: REMOVE_FROM_CART,
    payload: itemID,
  };
};

export const addToFavoritesAction = (item) => (dispatch, getState) => {
  dispatch({ type: ADD_TO_FAVORITES, payload: item });
  localStorage.setItem("favs", JSON.stringify(getState().shop.favorites));
};

export const removeFromFavoritesAction = (item) => (dispatch, getState) => {
  dispatch({ type: REMOVE_FROM_FAVORITES, payload: item });
  localStorage.setItem("favs", JSON.stringify(getState().shop.favorites));
};
export const adjustItemQtyAction = (itemID, qty) => {
  return {
    type: ADJUST_ITEM_QTY,
    payload: { id: itemID, qty },
  };
};
