import * as actionTypes from "./shopping-types";

const INITIAL_STATE = {
  isLoading: "",
  products: [],
  cart: [],
  favorites: localStorage.getItem('favs') ? JSON.parse(localStorage.getItem('favs')) : [],
  currentItem: {},
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case actionTypes.IS_LOADING:
      return {...state, isLoading: action.payload };

    case actionTypes.SET_PRODUCTS:
      return  { ...state, products: action.payload };

    case actionTypes.LOAD_CURRENT_ITEM:
      return { ...state, currentItem: action.payload};

    case actionTypes.ADD_TO_CART:
      //* Get Item-to-add to cart from products array
      //!action.payload === itemID!!!
      const itemToAdd = state.products.find(
        (product) => product.id === action.payload
      );
      //* Check if an Item is in cart already
      const inCart = state.cart.find(item => item.id === action.payload? true : false);
      //* if inCart true => return this cart item and add to qty +1, if false => do nothing
      //* if inCart false => add to cart[] new item (itemToAdd), qty=1
      return { ...state,
        cart: inCart ? state.cart.map((item) => item.id === action.payload
                ? { ...item, qty: item.qty + 1 }
                : item)
          : [...state.cart, { ...itemToAdd, qty: 1 }],
      };

    case actionTypes.ADJUST_ITEM_QTY:
      return { ...state, cart: state.cart.map((item) => item.id === action.payload.id
            ? { ...item, qty: +action.payload.qty }
            : item)
      };
    case actionTypes.ADD_TO_FAVORITES:
      const inFavs = state.favorites.find(item => item.id === action.payload.id);
      if (inFavs) return state
      return {...state, favorites: [...state.favorites, {...action.payload, qty: 1}]};

    case actionTypes.REMOVE_FROM_CART:
      return {...state, cart: state.cart.filter(item => item.id !== action.payload)};

    case actionTypes.REMOVE_FROM_FAVORITES:
      return { ...state,                                        //! action.payload is Item!
        favorites: state.favorites.filter(item => item !== action.payload)
      };
    default:
      return state;
  }
};

export default reducer;
