import React from "react";
import { Switch, Route} from "react-router-dom";
import "./App.css";
import Navbar from "./components/Navbar/Navbar";
import SingleItem from "./components/SingleItem/SingleItem";
import { connect } from "react-redux";
import Favorites from "./components/Favorites/Favorites";
import Products from "./components/Products/Products";
import Cart from "./components/Cart/Cart";
import PageNotFound from "./components/PageNotFound/PageNotFound";

function App ({ current }) {
  return (
    <div className="app">
      <Navbar />
      <Switch>
        <Route exact path="/" component={Products} />
        <Route path="/cart" component={Cart} />
        <Route path="/favorites" component={Favorites} />
        <Route exact path="/product/:id" component={SingleItem} />
        <Route path="*"><PageNotFound /></Route>
      </Switch>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    current: state.shop.currentItem,
  };
};

export default connect(mapStateToProps)(App);
