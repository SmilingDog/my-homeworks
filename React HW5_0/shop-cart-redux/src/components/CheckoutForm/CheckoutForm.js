import React from 'react'
import { withFormik } from 'formik';
import MyInput from './MyInput';
import schema from './schema';
import './CheckoutForm.css'
import { clearCartAction } from '../../redux/Shopping/shopping-actions';
import store from '../../redux/store';

function CheckoutForm({handleSubmit, isSubmitting}) {
  // console.log('formikForm2', props);

  return (
    <form onSubmit={handleSubmit} noValidate>
      <h3>Customer Info:</h3>
      <MyInput name='firstName' type='text' placeholder='First Name' />
      <MyInput name='lastName' type='text' placeholder='Last Name' />
      <MyInput name='age' type='number' placeholder='Age' />
      <MyInput name='address' type='text' placeholder='Delivery Address' />
      <MyInput name='phone' type='text' placeholder='Mobile number' />
      <div>
        <button
          className='summary__checkoutBtn'
          disabled={isSubmitting}
          type='submit'
        >
          Order Checkout
        </button>
      </div>
    </form>
  )
}
//*========= КОНЕЦ КОМПОНЕНТА ================
//* функция placeOrder() - за пределами компонента checkOutForm()
const logOrder = (arr) => {
  console.log('Your order:');
  arr.forEach(item => {
    console.log(`item: ${item.title}, quantity: ${item.qty}`);
  })
}


const placeOrder = (values, {setSubmitting}) => {
  console.log('Customer Info:', values);
  // console.log('helpers', helpers);
  setSubmitting(false);
  logOrder(store.getState().shop.cart)
  localStorage.setItem("cart", "[]")
  store.dispatch(clearCartAction())
}

export default withFormik ({
  mapPropsToValues: () => ({
    firstName: '',
    lastName: '',
    age: '',
    address: '',
    phone: ''
  }),
  handleSubmit: placeOrder,
  validationSchema: schema
}) (CheckoutForm)
