import React, { useState, useEffect } from "react";
import styles from "./Cart.module.css";

import { connect } from "react-redux";
import CartItem from "../CartItem/CartItem";
import CheckoutForm from "../CheckoutForm/CheckoutForm";


const Cart = ({ cart }) => {
  const [totalPrice, setTotalPrice] = useState(0);
  const [totalItems, setTotalItems] = useState(0);

  useEffect(() => {
    let quantity = 0;
    let price = 0;

    price = cart.reduce((a, item) => a + item.qty * item.price, 0);
    quantity = cart.reduce((a, item) => a + item.qty, 0);
    setTotalItems(quantity);
    setTotalPrice(price);
  }, [cart]);

  return (
    <div className={styles.cart}>
      <div className={styles.cart__items}>
        {!cart.length && <h3>Nothing in Cart now...</h3>}
        {cart.length > 0 && <h3> You are in Cart section...</h3>}
        {cart.length > 0 && cart.map((item) => (
          <CartItem key={item.id} item={item} />
        ))}
      </div>
      <div className={styles.cart__summary}>
        <h4 className={styles.summary__title}>Cart Summary</h4>
        <div className={styles.summary__price}>
          <span>TOTAL: ({totalItems} items)</span>
          <span>UAH {totalPrice}</span>
        </div>
        <CheckoutForm/>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    cart: state.shop.cart,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {

  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Cart);
