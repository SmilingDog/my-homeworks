"use strict"

let n = +prompt("Пож-та, введите число n ");

while (!n || isNaN(n)) {
   console.log("⛔️ Ошибка! Аргумент не является числом!");
   n = +prompt("Пож-та, введите целое число");
}

function factorial(n) {
   return (n === 1) ?  1 : n * factorial(n - 1) ;
}

console.log(`Факториал числа ${n} равен ${factorial(n)}`);
