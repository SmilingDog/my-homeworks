const slide = document.querySelector(".slide");
const images = document.querySelectorAll(".image-to-show");
const container = document.querySelector(".slider-container");



const btnStop = document.createElement("button");
btnStop.innerText = "Stop";
const btnGo = document.createElement("button");
btnGo.innerText = "Go";
const btns = document.createElement("div");
const timer = document.querySelector('span')

btns.classList.add("btns-container");
btnStop.classList.add("nice-button");
btnGo.classList.add("nice-button");

btns.append(btnStop, btnGo);
container.append(btns);

//* Counter of images === index of each image.
let counter = 0;
//* The width of 1 slide
const width = images[0].clientWidth;

let interval, interval2; //todo Здесь нужно объявить interval глобально, чтобы иметь к нему доступ из обеих функций
let time = 10;
timer.innerText = time

let delay = 10000


function startSlider() {
  clearInterval(interval) //todo Воизбежание многократного запуска setInterval() его нужно каждый раз очищать!!!
  clearInterval(interval2)


  interval = setInterval(() => {
    slide.classList.add("smooth");
    counter++;
    if (counter > images.length - 1) counter = 0;
    slide.style.transform = `translateX(-${width * counter}px)`;

  }, delay);


  interval2 = setInterval(() => {
    time--;
    console.log(time);
    if (time === 0) {
      time = 10;
      console.log('Testing if time == 0');
      delay = 10000 //*этот код тупо не выполняется, и я не  знаю почему....

    }
    timer.innerText = time

  }, 1000);
}

function stopSlider() {
  delay = time * 1000
  console.log(delay);
  clearInterval(interval);
  clearInterval(interval2)
}

startSlider(); //todo Запускаем слайдер сразу (автоматически) путем вызова функции

btnGo.addEventListener("click", startSlider);
btnStop.addEventListener("click", stopSlider);
