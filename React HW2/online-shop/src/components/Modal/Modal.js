import React, { Component } from "react";
import "./Modal.scss";
import PropTypes from "prop-types";
class Modal extends Component {
  render() {
    const {
      header,
      text,
      backgroundColor,
      color,
      action1,
      action2,
    } = this.props;

    const styles = {
      backgroundColor,
      color
    }

    return (
      <div style={styles} className="modal">
        <div>
          <div className="modal__header">
            <h2 className="modal__header__text">{header}</h2>
          </div>
          <p className="modal__text">{text}</p>
        </div>
        <div className='modal__btns-center'>
          {action1}
          {action2}
        </div>
      </div>
    );
  }
}
Modal.propTypes = {

  header: PropTypes.string,
  text: PropTypes.string,
  backgroundColor: PropTypes.string,
  color: PropTypes.string,
}
Modal.defaultProps = {
  header: "You added this item to the Cart",
  text: "Thank you for the interest in our products. Happy shopping!",
  backgroundColor: '#ffff00',
  color: '#000',
}
export default Modal;
