import { Component } from "react";
import Button from "../Button/Button";
import "./Card.scss";
import starBlack from "../../icons/star-black.svg";
import starGold from "../../icons/star-gold.svg";
import PropTypes from "prop-types";

class Card extends Component {
  state = {
    isSelected: false,
  };

  toggleStar = () => {
    const { isSelected } = this.state;
    const { article } = this.props;

    if (!isSelected) {
      localStorage.setItem(`card-${article}`, article);
    } else {
      localStorage.removeItem(`card-${article}`);
    }
    this.setState({ isSelected: !this.state.isSelected });
  };

  checkSelected = () => {
    const { article } = this.props;
    if (localStorage.getItem(`card-${article}`))
      this.setState({ isSelected: true });
  };
  componentDidMount = () => {
    this.checkSelected();
  };

  render() {
    const { title, price, imgpath, article, color, itemText } = this.props;
    const { isSelected } = this.state;
    return (
      <li className="card" colortype={color}>
        <img
          width="300"
          height="300"
          src={imgpath}
          alt={"item " + article}
          className="card__img">
        </img>
        <div className="card__info">
          <h3 className="card__title">{title}</h3>
          {isSelected && (
            <img src={starGold} alt="favorite item" onClick={this.toggleStar} />
          )}
          {!isSelected && (
            <img src={starBlack} alt="simple item" onClick={this.toggleStar} />
          )}
          <p className='item-text'><strong>Lorem ipsum</strong> {itemText}</p>
          <p className="card__article">{"Item № : " + article}</p>
        </div>
        <div className='price-cart'>
          <p className="card__price">{price}</p>
          <Button />
        </div>
      </li>
    );
  }
}
Card.propTypes = {
  selected: PropTypes.bool,
  title: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  color: PropTypes.string,
  imgpath: PropTypes.string.isRequired,
  article: PropTypes.string.isRequired,
  itemText: PropTypes.string,
};
Card.defaultProps = {
  itemText: "dolor sit amet, com adipiscing elit, sed diam nonu.",
};

export default Card;
