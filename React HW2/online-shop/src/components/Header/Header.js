import  { PureComponent } from "react";
import "./Header.scss";

class Header extends PureComponent {
  render() {
    return (
      <>
        <div>
          <img
            src="https://cameralabs.org/media/camera/may/23may2/33_2191ca4565c077d53c32165a244117fd.jpg"
            alt="header banner"
            className="page-banner">
          </img>
        </div>
        <h1 className="main-title"> Welcome to our Sport Goods shop!</h1>
      </>
    );
  }
}

export default Header;
