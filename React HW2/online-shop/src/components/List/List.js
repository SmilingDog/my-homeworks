import { Component } from "react";
import Card from "../Card/Card";
import axios from "axios";
import "./List.scss";
import PropTypes from 'prop-types';

class List extends Component {
  state = {
    goods: [],
    isLoading: true,
  };

  componentDidMount() {
    axios("/goods.json").then((res) =>
      this.setState({ goods: res.data, isLoading: false })
    );
  }

  render() {
    const { goods, isLoading } = this.state;

    if (isLoading) return false;

    const goodsItems = goods.map((el) => (
      <Card
        key={el.article}
        title={el.title}
        price={el.price}
        imgpath={el.url}
        color={el.color}
        article={el.article}
      />
    ));

    return (
      <ol className='list'>
       {goodsItems}
      </ol>
    )
  }
}
List.propTypes = {
  goods:PropTypes.array,
  isLoading:PropTypes.bool,
  goodsItems:PropTypes.array

}
export default List;
