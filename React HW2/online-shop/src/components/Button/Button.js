import React, { Component } from "react";
import Modal from "../Modal/Modal";
import "./Button.scss";
import PropTypes from "prop-types";

class Button extends Component {
  state = {
    showModal: false
  };
  openModal = () => {
    this.setState({ showModal: true });
  };
  closeModal = () => {
    this.setState({ showModal: false });
  };

  render() {
    const { showModal } = this.state;

    const modalBtnOk = <button className="modal__btn" onClick={this.closeModal}>OK</button>;
    const modalBtnCancel = <button className="modal__btn" onClick={this.closeModal}>Cancel</button>;

    return (
      <>
        {!showModal && (
          <button onClick={this.openModal} className="btn">
            Add to Cart
          </button>
        )}
        {showModal && (
          <button onClick={this.closeModal} className="btn">
            Remove from Cart
          </button>
        )}
        {showModal && <Modal action1={modalBtnOk} action2={modalBtnCancel} />}
      </>
    );
  }
}
Button.propTypes = {

  modalBtnOk: PropTypes.element,
  modalBtnCancel: PropTypes.element,
};

export default Button;
