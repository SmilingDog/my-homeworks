'use strict'


const books = [
    {
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70
    },
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    },
    {
      name: "Тысячекратная мысль",
      price: 70
    },
    {
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    },
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    }
  ];


const root = document.getElementById('root');
const ul = document.createElement('ul');
root.append(ul);

books.forEach((el, indx) => {
    const li = document.createElement('li');
    try {
        for (let key in el) {
            const liContent = document.createElement('p');
            liContent.textContent = key +': ' + el[key];
            li.append(liContent);
        }
        if (!el.author) {
            throw new Error(`Incomplete data:  NO book Author in element #${indx+1}`);
        } if (!el.name) {
            throw new Error(`Incomplete data:  NO book Name in element #${indx+1}`);
        } if (!el.price) {
            throw new Error(`Incomplete data:  NO book Price in element #${indx+1}`);
        }
        ul.append(li);
    }
    catch (err) {
        console.log(err);
    }
})
