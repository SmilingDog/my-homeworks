const parent = document.querySelector(".images-wrapper");
const btn = document.getElementById("button");
const containers = document.querySelectorAll(".img-container");
const images = document.querySelectorAll(".image-to-show");
const texts = document.querySelectorAll(".image-text");

containers.forEach((container) => {
  container.addEventListener("click", (e) => {
    let clickedEl = e.target;
    clickedEl.classList.add("fade");
    if (clickedEl.className == "image-to-show fade") {
      clickedEl.nextElementSibling.classList.add("fade");
    } else {
      clickedEl.previousElementSibling.classList.add("fade");
    }
  });
});

btn.addEventListener("click", () => {
  const elements = [...images, ...texts];
  elements.forEach((img) => img.classList.remove("fade"));
});
