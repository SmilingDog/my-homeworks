const root = document.getElementById('root');
const btn = document.getElementById('btn');

const ipURL = "https://api.ipify.org/?format=json";
const locationURL = "http://ip-api.com/json/"


btn.addEventListener('click', iWillCalculateYou);

//===========================================================
// 1-способ: через async/await

// async function iWillCalculateYou() {
//     try {
//     const response = await fetch(ipURL);
//     const {ip} = await response.json();
//    
//     const url = locationURL + ip;
//     const reply = await fetch(url);
//     const userData = await reply.json();

//     renderUserData(ip, userData);

//     } catch (e) {
//         console.error(e);
//     }

// }
//===================================================
// 2-cпособ: тоже самое через fetch/then

function iWillCalculateYou() {
    return fetch(ipURL)
        .then(response => response.json())
        .then((userIp) => {
            const{ip} = userIp;
            const url = locationURL + ip;
            fetch(url)
            .then(reply => reply.json())
            .then(userData => renderUserData(ip, userData));
        })
        .catch(err => console.error(err))
}
//=======================================================
// 3-способ: тоже самое через axios

// function iWillCalculateYou() {
//     axios
// 	.get(ipURL)
// 	.then(({data}) => {
//         const{ip} = data;
//         const url = locationURL + ip;
//     axios
//     .get(url)
//     .then(({data}) => renderUserData(ip, data))
// 	})
// 	.catch(error => console.error(error))

// }
//========================================================
    const renderUserData = function(ip, userData) {
    root.textContent = '';
    const h3 = document.createElement('h3');
    h3.textContent = `Your IP address: ${ip}`;
    const ul = document.createElement('ul');
    ul.style.listStyle = 'none';
    root.append(h3, ul);

    for (let key in userData) {
        const li = document.createElement('li');
        li.textContent = `${key} : ${userData[key]}`;
        ul.append(li);
    }
}
