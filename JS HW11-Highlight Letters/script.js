"use strict";

const buttons = document.querySelectorAll(".btn");

document.addEventListener("keydown", (event) => {
  console.log(event.key);
  buttons.forEach((item) => {
    item.style.backgroundColor = "#33333a"; //*сброс цвета всем кнопкам перед выбором

    if (event.key == item.getAttribute("data-focus")) {
      item.style.backgroundColor = "blue";
    }
  });
});
