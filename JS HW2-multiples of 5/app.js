"use strict";

// let num = +prompt("Please enter the last number");
// while(!Number.isInteger(num)) {
//     num = +prompt("Number must be integer");
// }

// if (num < 5) {
//     console.log("Sorry, no numbers");
// }
// for (let i = 5; i <= num; i++) {
//     if (i % 5 == 0) {
//         console.log(i);
//     }
// }

let n = +prompt("Number for prime check");

function isPrime(n) {
  for (let i = 2; i < n; i++) {
    if (n % i == 0) return false;
  }
  return true;
}
console.log(isPrime(n));

let m = +prompt("Enter  the limit");

function primeNumberFilter(m) {
  for (let i = 2; i <= m; i++) {
    if (isPrime(i) == true) {
      console.log(i);
    }
  }
}
primeNumberFilter(m);
//todo Функции которые ничего не возвращают надо просто вызывать. Console.log(Функция без return) выдаст undefined
