"use strict"

function fiboPositive (n, F0, F1) {
   if (n === 2){
       return F1;
   } else if (n === 1){
       return F0;
   } else {
       let f1 = F0, f2 = F1, f3;
       for (let i = 1; i < n; i++) {
           f3 = f1 + f2;
           f1 = f2;
           f2 = f3;
       }
      return f3;
   }
}
function fiboNegative (n, F0, F1) {

   if (n < 0) {
      let f1 = F0, f2 = F1, f3;
      for (let i = -1; i > n; i--) {
         f3 = f1 - f2;
         f1 = f2;
         f2 = f3;
      }
      return f3;
   }
}

let F0 = +prompt(" Please Enter F0");
   while(F0 == null || F0 == undefined || F0 == "" || isNaN(+F0)) {
       F0 = +prompt("Enter valid F0");
   }

let F1 = +prompt("Please Enter F1");
   while(F1 == null || F1 == undefined || F1 == "" || isNaN(+F1)) {
       F1 = +prompt("Enter valid F1");
   }

let n = +prompt(" Please Enter n");
   while(n == null || n == undefined || n == "" || isNaN(+n)) {
       n = +prompt("Enter valid n");
   }

if (F0 > 0 && F1 > 0){
console.log(`Fibonacci of n is: ${fiboPositive(n, F0, F1)}`);
} else {
console.log(`Fibonacci of n is: ${fiboNegative(n, F0, F1)}`);
}
