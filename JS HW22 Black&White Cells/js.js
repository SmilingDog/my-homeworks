const table = document.querySelector(".content-table");
const body = document.body;
table.addEventListener("click", (e) => {
  e.target.classList.toggle('active')
});

body.addEventListener("click", (e) => {
  let isClickInsideTable = table.contains(e.target);

  if (!isClickInsideTable) {
    const cells = document.querySelectorAll("td");
    cells.forEach((c) => c.classList.toggle("active"));
  }
});
