import React, { useState, useEffect } from "react";
import { Route, Switch } from "react-router-dom";
import Favorites from "../components/Favorites/Favorites";
import Basket from "../components/Basket/Basket";
import Main from "../components/Main/Main";
import axios from "axios";

const AppRoutes = () => {
  // localStorage.clear()

  const [products, setProducts] = useState([]); //* можно передавать и переменную и Сеттер-функцию как  props дальше

  const basketLocal = JSON.parse(localStorage.getItem('basketItems')) || [];
  const [basketItems, setBasketItems] = useState(basketLocal);

  const favsLocal = JSON.parse(localStorage.getItem('favItems')) || [];
  const [favoriteItems, setFavoriteItems] = useState(favsLocal);

  const favIdLocal = JSON.parse(localStorage.getItem('favId')) || [];
  const [favId, setFavId] = useState(favIdLocal);

  const cartIdLocal = JSON.parse(localStorage.getItem('cartId')) || [];
  const [cartId, setCartId] = useState(cartIdLocal);

  const [itemId, setItemId] = useState(null);
  const [itemName, setItemName] = useState(null);
  const [showModal, setShowModal] = useState(false);


  useEffect(() => {
    axios("/products.json").then((res) => {
      setProducts(res.data)
    })
    localStorage.setItem("basketItems", JSON.stringify(basketItems));
    localStorage.setItem("favItems", JSON.stringify(favoriteItems));
    localStorage.setItem('favId', JSON.stringify(favId))
    localStorage.setItem('cartId', JSON.stringify(cartId))
  }, [basketItems, favoriteItems, favId, cartId]);

  const openModal = (item) => {
    setShowModal(true);
    setItemId(item.id);
    setItemName(item.name);
  };
  const openModalFavorites = (item) => {
    setShowModal(true);
    setItemId(item.id);
    setItemName(item.name);
  };
  const closeModal = () => {
    setShowModal(false);
  };

  const onAdd = (product) => {
    setCartId([...cartId, +product.id])

    const existInBasket = basketItems.find(
      (basketItem) => basketItem.id === product.id
    );

    if (existInBasket) {
      setBasketItems(
        basketItems.map((basketItem) =>
          basketItem.id === product.id
            ? { ...existInBasket, qty: existInBasket.qty + 1 }
            : basketItem
        )
      );
    } else {
      setBasketItems([...basketItems, { ...product, qty: 1 }]);
    }

  };

  const onRemove = (product) => {
    const existInBasket = basketItems.find(
      (basketItem) => basketItem.id === product.id
    );
    if (existInBasket.qty === 1) {
      setBasketItems(
        basketItems.filter((basketItem) => basketItem.id !== product.id)
      );
    } else {
      setBasketItems(
        basketItems.map((basketItem) =>
          basketItem.id === product.id
            ? { ...existInBasket, qty: existInBasket.qty - 1 }
            : basketItem
        )
      );
    }
  };

  const onStar = (product) => {
    setFavId([...favId, +product.id]);

    const existInFavs = favoriteItems.find(
      (favoriteItem) => favoriteItem.id === product.id)

    if (existInFavs) {
      setFavoriteItems([...favoriteItems])
    } else {
    setFavoriteItems([...favoriteItems, { ...product}]);
    }
  };

  const onDelete = (id) => {
    setBasketItems(basketItems.filter((basketItem) => basketItem.id !== id));
    setCartId(cartId.filter((item) => item !== +id));
    setShowModal(false);
  };

  const onDeleteFavorites = (id) => {
    setFavoriteItems(favoriteItems.filter((favoriteItem) => favoriteItem.id !== id));
    setFavId(favId.filter((item) => item !== +id)); //todo Не забывать про конвертацию String to Number!!!! цифры приходят в виде строк!!! и ничего не работает без приведения типов +id
    setShowModal(false);
  }

  return (
    <Switch>
      <Route exact path="/">
        <Main
          favId={favId}
          cartId={cartId}
          onAdd={onAdd}
          onStar={onStar}
          products={products}

        />
      </Route>
      <Route path="/basket">
        <Basket
          onAdd={onAdd}
          onRemove={onRemove}
          onDelete={onDelete}
          openModal={openModal}
          closeModal={closeModal}
          showModal={showModal}
          itemId={itemId}
          itemName={itemName}
        />
      </Route>
      <Route path="/favorites">
        <Favorites

          openModalFavorites={openModalFavorites}
          closeModal={closeModal}
          onDeleteFavorites={onDeleteFavorites}
          showModal={showModal}
          itemId={itemId}
          itemName={itemName}
        />
      </Route>
    </Switch>
  );
};

export default AppRoutes;
