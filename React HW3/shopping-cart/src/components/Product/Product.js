//import React from "react";
import Star from "../Star/Star";
import React from "react";

const Product = ({ product, onAdd, onStar, isFavorite, inCart}) => {

return (
    <div>
      <img className="middle" src={product.image} alt={product.name}></img>
      <h3>{product.name}</h3>
      { isFavorite && <div onClick={() => onStar(product)}><Star filled /></div>}
      { !isFavorite && <div onClick={() => onStar(product)}><Star filled={false} /></div>}
      <div>{product.price} грн.</div>
      <div>
        {inCart && <button className='btn-in' onClick={() => onAdd(product)}>In Cart</button>}
        {!inCart && <button className='btn' onClick={() => onAdd(product)}>Add to Cart</button>}
      </div>
    </div>
  );
};
export default Product;
