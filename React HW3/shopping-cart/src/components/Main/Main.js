import React from "react";
import Product from "../Product/Product";

const Main = ({ onAdd, onStar, products, favId, cartId }) => {

  const productItems = products.map((product) =>
    cartId.includes(+product.id) ?  //*Метод includes() определяет, содержит ли массив определённый элемент, возвращая в зависимости от этого true или false.
      <Product
        key={product.id}
        product={product}
        onAdd={onAdd}
        onStar={onStar}
        isFavorite={favId.includes(+product.id)}
        inCart
      />
     :
      <Product
        key={product.id}
        product={product}
        onAdd={onAdd}
        onStar={onStar}
        isFavorite={favId.includes(+product.id)}
        InCart={false}
      />
    
  );

  return (
    <main className="block">
      <h2>Products</h2>
      <div className="row">{productItems}</div>
    </main>
  );
};
export default Main;
