import React from "react";
import { NavLink } from "react-router-dom";

const Header = () => {
  return (
    <header className="row block">
      <NavLink exact to="/" className="link" activeClassName="link active" >
        <h1>Welcome to 'Sportify' shop!</h1>
      </NavLink>
      <div className='cart-fav'>
        <NavLink exact to="/basket" className="link" activeClassName="link active" >
          My Basket
        </NavLink>
        <NavLink exact to="/favorites" className="link" activeClassName="link active" >
          My Favorites
        </NavLink>
      </div>
    </header>
  );
};
export default Header;
