import PropTypes from "prop-types";

const Modal = ({ header, text, backgroundColor, color, onDelete, closeModal, itemId, itemName}) => {

  const styles = {
    backgroundColor,
    color,
  };

  return (
    <div style={styles} className="modal">
      <div>
        <div className="modal__header">
          <h2 className="modal__header__text">{header}</h2>
          <h3>{itemName}</h3>
        </div>
        <p className="modal__text">{text}</p>
      </div>

      <div className="modal__btns-center">
        <button className="modal__btn" onClick={() => onDelete(itemId)}>OK</button>
        <button className="modal__btn" onClick={closeModal}>Cancel</button>
      </div>

    </div>
  );
};

Modal.propTypes = {
  header: PropTypes.string,
  text: PropTypes.string,
  backgroundColor: PropTypes.string,
  color: PropTypes.string,
};
Modal.defaultProps = {
  header: "You are going to clear this Basket item:",
  text: "Are you sure?",
  backgroundColor: "#ffff00",
  color: "#000",
};
export default Modal;
