const BasketItem = ({ id, index, name, image, qty, price, onAdd, onRemove, openModal, item }) => {

  return (
    <div key={id} className="row">
      <span>{index + 1}</span> <div className="col-2">{name}</div>
      <img className="small" src={image} alt={name}></img>
      <div className="col-2">
        <button onClick={() => onAdd(item)} className="btn__add">
          add
        </button>
        <button onClick={() => onRemove(item)} className="btn__remove">
          del
        </button>
        <button onClick={() => openModal(item)} className="btn__clear">
          clear item
        </button>
      </div>
      <div className="col-2">
        {+qty} X {+price} грн.
      </div>
      <div className="col-2">Total: {qty * price}</div>

    </div>
  );
};
 export default BasketItem
