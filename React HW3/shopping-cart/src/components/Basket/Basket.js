import React from "react";
import BasketItem from "../BasketItem/BasketItem";
import Modal from "../Modal/Modal";

const Basket = ({
  onAdd,
  onRemove,
  showModal,
  openModal,
  onDelete,
  closeModal,
  itemId,
  itemName,
}) => {
  const basketItems = JSON.parse(localStorage.getItem("basketItems"));
  const total = basketItems.reduce((a, item) => a + item.qty * item.price, 0);

  const basketItemsList = basketItems.map((item, index) => (
    <BasketItem
      key={item.id}
      index={index}
      {...item}
      onAdd={onAdd}
      onRemove={onRemove}
      openModal={openModal}
      item={item}
    />
  ));
  return (
    <section className="block col-1">
      <h2> Basket Items</h2>
      {basketItems.length === 0 && <div>Basket is empty</div>}
      <div>{basketItemsList}</div>
      {total > 0 && <h3 className="text-center">Total Basket: {total} грн.</h3>}
      {showModal && (
        <Modal
          onDelete={onDelete}
          closeModal={closeModal}
          itemId={itemId}
          itemName={itemName}
        />
      )}
    </section>
  );
};
export default Basket;
