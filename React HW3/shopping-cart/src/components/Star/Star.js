import React from "react";

function Star({ color = "yellow", filled }) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="44"
      height="44"
      viewBox="0 0 44 44"
    >
      <path
        d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z"
        stroke={color}
        fill={filled ? "gold" : "black"}
      />
    </svg>
  );
}
export default Star;
