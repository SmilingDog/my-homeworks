import React from "react";
import ModalFavorites from "../ModalFavorites/ModalFavorites";
import Star from "../Star/Star";

const Favorites = ({ onDeleteFavorites, openModalFavorites, showModal, closeModal, itemId, itemName}) => {

  const favItems = JSON.parse(localStorage.getItem("favItems"))

  
  return (
    <section className="block">
      <h2> Favorite Items</h2>
      {favItems.length === 0 && <div>No Favorite Items</div>}
      <div className="row">
        {favItems.map((item) => (
          <div key={item.id}>
            <img className="middle" src={item.image} alt={item.name}></img>
            <h3>{item.name}</h3>
            <div><Star filled /></div>
            <div>{item.price} грн.</div>
            <button onClick={() => openModalFavorites(item)} className="btn__clear">delete</button>
            {showModal && <ModalFavorites onDeleteFavorites={onDeleteFavorites} closeModal={closeModal} itemId={itemId} itemName={itemName}/>}
          </div>
        ))}
      </div>
    </section>
  );
};

export default Favorites;
