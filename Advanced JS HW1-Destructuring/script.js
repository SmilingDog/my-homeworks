"use strict"


// ==============================  Задание 1  ================================//
// Две компании решили объединиться, и для этого им нужно объединить базу данных своих клиентов.
// У вас есть 2 массива строк, в каждом из них - фамилии клиентов.
// Создайте на их основе один массив, который будет представлять собой объединение двух массивов без повторяющихся фамилий клиентов.
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Гилберт", "Зальцман", "Сальваторе", "Майклсон"];

// 1 способ - с помощью Set Constructor;
let united = [...clients1, ...clients2];

let mySet = new Set(united);
console.log([...mySet]);



//2 способ - с помощью  функции
// function onlyUniqueElems(array) {
//     let result = [];

//     for (let client of array) {
//       if (!result.includes(client)) {
//         result.push(client);
//       }
//     }
//     return result;
// }
// console.log(onlyUniqueElems(united));


//==============================================================================//
// ======================================  Задание 2 ===========================//

// Перед вами массив characters, состоящий из объектов. Каждый объект описывает одного персонажа.
// Создайте на его основе массив charactersShortInfo, состоящий из объектов, в которых есть только 3 поля - name, lastName и age.

const characters = [
    {
      name: "Елена",
      lastName: "Гилберт",
      age: 17,
      gender: "woman",
      status: "human"
    },
    {
      name: "Кэролайн",
      lastName: "Форбс",
      age: 17,
      gender: "woman",
      status: "human"
    },
    {
      name: "Аларик",
      lastName: "Зальцман",
      age: 31,
      gender: "man",
      status: "human"
    },
    {
      name: "Дэймон",
      lastName: "Сальваторе",
      age: 156,
      gender: "man",
      status: "vampire"
    },
    {
      name: "Ребекка",
      lastName: "Майклсон",
      age: 1089,
      gender: "woman",
      status: "vampire"
    },
    {
      name: "Клаус",
      lastName: "Майклсон",
      age: 1093,
      gender: "man",
      status: "vampire"
    }
];
// 1-ЫЙ СПОСОБ - С ПОМОЩЬЮ .MAP()
let charactersShortInfo = characters.map((el) => {//мэпом  перебираем все элементы characters;
  let {name, lastName, age}= el; //деструктуризируем каждый элемент(объект) characters на нужные нам поля;
  return {name, lastName, age }; // возвращается новый массив charactersShortInfo объектов только с 3 нужными полями;
});

console.log(charactersShortInfo);

// 2-ОЙ СПОСОБ - С ПОМОЩЬЮ ФУНКЦИИ
function shortify (arr) {
  let charactersShortInfo = [];
  for (let el of arr) {
    let {name, lastName, age} = el;
    let short = {name, lastName, age};
    charactersShortInfo.push(short);
  }
  return charactersShortInfo;
}

console.log(shortify(characters));

//==============================================================================================//

//============================ Задание 3 =======================================================//
//У нас есть объект user1:

const user1 = {
  name: "John",
  years: 30
};
// Напишите деструктурирующее присваивание, которое:
// свойство name присвоит в переменную name
// свойство years присвоит в переменную age
// свойство isAdmin присвоит в переменную isAdmin (false, если нет такого свойства)
// Выведите переменные на экран.

let {name, years:age, isAdmin = 'false'} = user1;

console.log(name);
console.log(age);
console.log(isAdmin);
//===============================================================================================//

//============================= Задание 4 =======================================================//

// Детективное агентство несколько лет собирает информацию о возможной личности Сатоши Накамото. Вся информация, собранная в конкретном году, хранится в отдельном объекте. Всего таких объектов три - satoshi2018, satoshi2019, satoshi2020.
// Чтобы составить полную картину и профиль, вам необходимо объединить данные из этих трех объектов в один объект - fullProfile.
// Учтите, что некоторые поля в объектах могут повторяться. В таком случае в результирующем объекте должно сохраниться значение, которое было получено позже (например, значение из 2020 более приоритетное по сравнению с 2019).
// Напишите код, который составит полное досье о возможной личности Сатоши Накамото. Изменять объекты satoshi2018, satoshi2019, satoshi2020 нельзя.

const satoshi2018 = {
  name: 'Satoshi',
  surname: 'Nakamoto',
  technology: 'Bitcoin',
  country: 'Japan',
  browser: 'Tor',
  birth: '1975-04-05'
}

const satoshi2019 = {
  name: 'Dorian',
  surname: 'Nakamoto',
  age: 44,
  hidden: true,
  country: 'USA',
  wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
  browser: 'Chrome'
}

const satoshi2020 = {
  name: 'Nick',
  surname: 'Sabo',
  age: 51,
  country: 'Japan',
  birth: '1979-08-21',
  location: {
    lat: 38.869422,
    lng: 139.876632
  }
}


let fullProfile = {...satoshi2018, ...satoshi2019, ...satoshi2020};
console.log(fullProfile);
console.log(satoshi2018);
//===========================================================================================//

//================================ Задание 5 =================================================//
// Дан массив книг. Вам нужно добавить в него еще одну книгу, не изменяя существующий массив (в результате операции должен быть создан новый массив).

const books =
[{
  name: 'Harry Potter',
  author: 'J.K. Rowling'
},
{
  name: 'Lord of the rings',
  author: 'J.R.R. Tolkien'
},
{
  name: 'The witcher',
  author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
  name: 'Game of thrones',
  author: 'George R. R. Martin'
}

let newBooks = books.concat(bookToAdd);
console.log(newBooks);
console.log(books);

//===========================================================================================//

//=================================   Задание 6  ================================================//
// Дан объект employee. Добавьте в него свойства age и salary, не изменяя изначальный объект (должен быть создан новый объект, который будет включать все необходимые свойства). Выведите новосозданный объект в консоль.

const employee = {
  firstName: 'Vitalii',
  surname: 'Klichko'
}

let { firstName, surname, years = 49, salary = '1 UAH'} = employee;

const newEmployee = {firstName, surname, years, salary};

console.log(newEmployee);

//===============================================================================================//

//================================  Задание 7  ==================================================//
// Дополните код так, чтоб он был рабочим

const array = ['value', () => 'showValue'];

let [value, showValue] = array;

alert(value); // должно быть выведено 'value'
alert(showValue());  // должно быть выведено 'showValue'
