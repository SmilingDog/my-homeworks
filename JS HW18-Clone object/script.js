"use strict"

let person = {
    firstName: "Max",
    lastName: "Kudryashov",
    age: 18,
    employments: ["компания Марш", "KFC", "Pizza Domino's"],
    "likes computers":  {
        samsung: true,
        lenovo: false,
        htc: false,
        xiaomi: true
    }
}

//const refToPerson = person;

function deepClone(obj) {
    const clObj = {};
    for(const i in obj) {
      if (obj[i] instanceof Object) {
        clObj[i] = deepClone(obj[i]);
      }
      else if (Array.isArray(obj[i])){
        clObj[i]=arrayCopy();

        function arrayCopy(){
          const array = [];
          obj[i].forEach((value, ind) => { array[ind] = value});
          return array
        }
        continue;
      }
      clObj[i] = obj[i];
    }
    return clObj;
}
const clonedPerson = deepClone(person);
clonedPerson.employments.push("DAN-IT");

console.log('Original object after cloning ', person);
console.log('Cloned object after cloning ',clonedPerson);
