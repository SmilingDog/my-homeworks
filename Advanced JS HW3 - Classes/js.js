'use strict'

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    set name (name) {
        this._name = name;
    }
    get name () {
        return this._name;
    }
    set age (age) {
        this._age = age;
    }
    get age () {
        return this._age;
    }
    set salary (salary) {
        this._salary = salary;
    }
    get salary () {
        return this._salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    set salary(salary) {
        this._salary = 'UAH '+ +salary.slice(4)*3;

    }
    get salary() {
        return this._salary;
    }
}

let employee = new Employee ('Lexa', 46, 'UAH 7000');
console.log(employee);
console.log(employee.age);
console.log(employee.salary);
console.log(employee.name);

let programmer1 = new Programmer ('Maximka', 19, 'UAH 2000', ['Ukrainian', 'English', 'Russian']);
console.log(programmer1);
console.log(programmer1.age);
console.log(programmer1.salary);
console.log(programmer1.name);

let programmer2 = new Programmer ('Sasha', 29, 'UAH 5000', ['Ukrainian', 'German', 'Russian']);
console.log(programmer2);
console.log(programmer2.age);
console.log(programmer2.salary);
console.log(programmer2.name);

let programmer3 = new Programmer ('Lionya', 25, 'UAH 8000', ['Ukrainian', 'English', 'Marsian']);
console.log(programmer3);
console.log(programmer3.age);
console.log(programmer3.salary);
console.log(programmer3.name);
